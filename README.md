# raspberry_pico_w_websocket

Displays a changing value (random percentage in this case) on a web page using web sockets.
Based on https://github.com/BetaRavener/upy-websocket-server

## Getting started

Works with Raspberry Pi Pico W.

Change variables 'ssid' and 'password' from main.py to match your local WIFI.
Copy all files on Raspberry Pi Pico W and start main.py.

To get the assigned IP check the console for something like:
```
...
connected
ip = 192.168.100.66
```

Visit http://ip-assigned-to-your-raspberry:3000


Remark: no additional libraries needed; Just make sure to install the recommended version of python interpreter (e.g. rp2-pico-w-20221220-unstable-v1.19.1-782-g699477d12.uf2)