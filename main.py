import network
import time
import random

from ws_connection import ClientClosedError
from ws_server import WebSocketServer, WebSocketClient

ssid = 'SSIDD'
password = 'PASSWORD'

class ValueGenerator(WebSocketClient):
    value = 0
    def __init__(self, conn):
        super().__init__(conn)

    def process(self):
        try:
            previousValue = self.value
            self.value = random.randint(40, 80)
            if previousValue != self.value:
                self.connection.write(str(self.value))
            
        except ClientClosedError:
            self.connection.close()


class AppServer(WebSocketServer):
    def __init__(self):
        super().__init__("percentage.html", 10)

    def _make_client(self, conn):
        return ValueGenerator(conn)

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(ssid, password)

max_wait = 10
while max_wait > 0:
    if wlan.status() < 0 or wlan.status() >= 3:
        break
    max_wait -= 1
    print('waiting for connection...')
    time.sleep(1)

if wlan.status() != 3:
    raise RuntimeError('network connection failed')
else:
    print('connected')
    status = wlan.ifconfig()
    print( 'ip = ' + status[0] )

server = AppServer()
server.start(3000)
try:
    while True:
        server.process_all()
        time.sleep(0.3)
except KeyboardInterrupt:
    pass
server.stop()
